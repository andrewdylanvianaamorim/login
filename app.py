from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for
import sql

app = Flask("App")

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=["GET", "POST"])
def login():
    if request.method == "GET":
        return render_template('login.html')
    else:
        matricula = request.form["txt_login"]
        senha = request.form["senha_login"]
        aluno = sql.pegar_aluno_por_matricula(matricula)

        if len(aluno) == 0 or aluno[0][2] != senha:
            return redirect(url_for("login_falha"))

        return redirect(url_for("usuario", matricula=matricula))

@app.route('/login/falha', methods=["GET", "POST"])
def login_falha():
    if request.method == "GET":
        return render_template('login_falha.html')
    else:
        matricula = request.form["txt_login"]
        senha = request.form["senha_login"]
        aluno = sql.pegar_aluno_por_matricula(matricula)

        if len(aluno) == 0 or aluno[0][2] != senha:
            return redirect(url_for("login_falha"))

        return redirect(url_for("usuario", matricula=matricula))

@app.route('/cadastro', methods=["GET", "POST"])
def method_name():
    return render_template('cadastro.html')

@app.route('/login/<matricula>')
def usuario(matricula):
    return f"{matricula}"

if __name__ == "__main__":
    app.run(debug=True)
    sql.finalizar_conexao()